<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLetraquenardLetraquenardSemestre extends Migration
{
    public function up()
    {
        Schema::create('letraquenard_letraquenard_semestre', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('libelle');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('letraquenard_letraquenard_semestre');
    }
}
