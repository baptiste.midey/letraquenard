<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLetraquenardLetraquenardUser2 extends Migration
{
    public function up()
    {
        Schema::table('letraquenard_letraquenard_user', function($table)
        {
            $table->string('mail');
            $table->string('mdp');
            $table->integer('letraquenard_letraquenard_promotion_id')->nullable();
            $table->string('nom')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('prenom')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('letraquenard_letraquenard_user', function($table)
        {
            $table->dropColumn('mail');
            $table->dropColumn('mdp');
            $table->dropColumn('letraquenard_letraquenard_promotion_id');
            $table->text('nom')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('prenom')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
