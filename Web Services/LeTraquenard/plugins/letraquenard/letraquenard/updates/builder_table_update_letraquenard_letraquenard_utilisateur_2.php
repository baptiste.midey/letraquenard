<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLetraquenardLetraquenardUtilisateur2 extends Migration
{
    public function up()
    {
        Schema::table('letraquenard_letraquenard_utilisateur', function($table)
        {
            $table->renameColumn('letraquenard_letraquenard_promotion_id', 'promotion_id');
        });
    }
    
    public function down()
    {
        Schema::table('letraquenard_letraquenard_utilisateur', function($table)
        {
            $table->renameColumn('promotion_id', 'letraquenard_letraquenard_promotion_id');
        });
    }
}
