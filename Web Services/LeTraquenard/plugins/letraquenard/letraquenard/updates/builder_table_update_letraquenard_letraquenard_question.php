<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLetraquenardLetraquenardQuestion extends Migration
{
    public function up()
    {
        Schema::table('letraquenard_letraquenard_question', function($table)
        {
            $table->integer('matiere_id');
        });
    }
    
    public function down()
    {
        Schema::table('letraquenard_letraquenard_question', function($table)
        {
            $table->dropColumn('matiere_id');
        });
    }
}