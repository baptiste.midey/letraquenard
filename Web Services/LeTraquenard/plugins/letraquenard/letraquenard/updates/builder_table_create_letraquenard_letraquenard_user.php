<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLetraquenardLetraquenardUser extends Migration
{
    public function up()
    {
        Schema::create('letraquenard_letraquenard_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->text('nom');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('letraquenard_letraquenard_user');
    }
}
