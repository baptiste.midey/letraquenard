<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLetraquenardLetraquenardRepondre extends Migration
{
    public function up()
    {
        Schema::create('letraquenard_letraquenard_repondre', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('vraifaux');
            $table->integer('question_id');
            $table->integer('semestre_id');
            $table->integer('utilisateur_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('letraquenard_letraquenard_repondre');
    }
}
