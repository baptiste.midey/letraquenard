<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLetraquenardLetraquenardUtilisateur extends Migration
{
    public function up()
    {
        Schema::rename('letraquenard_letraquenard_user', 'letraquenard_letraquenard_utilisateur');
    }
    
    public function down()
    {
        Schema::rename('letraquenard_letraquenard_utilisateur', 'letraquenard_letraquenard_user');
    }
}
