<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLetraquenardLetraquenardPromotion extends Migration
{
    public function up()
    {
        Schema::create('letraquenard_letraquenard_promotion', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('libelle');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('letraquenard_letraquenard_promotion');
    }
}
