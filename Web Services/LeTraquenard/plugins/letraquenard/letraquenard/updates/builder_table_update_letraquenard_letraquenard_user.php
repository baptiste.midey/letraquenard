<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLetraquenardLetraquenardUser extends Migration
{
    public function up()
    {
        Schema::table('letraquenard_letraquenard_user', function($table)
        {
            $table->text('prenom');
        });
    }
    
    public function down()
    {
        Schema::table('letraquenard_letraquenard_user', function($table)
        {
            $table->dropColumn('prenom');
        });
    }
}
