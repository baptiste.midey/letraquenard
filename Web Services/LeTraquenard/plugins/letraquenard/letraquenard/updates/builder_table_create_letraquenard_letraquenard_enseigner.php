<?php namespace Letraquenard\Letraquenard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLetraquenardLetraquenardEnseigner extends Migration
{
    public function up()
    {
        Schema::create('letraquenard_letraquenard_enseigner', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('promotion_id');
            $table->integer('matiere_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('letraquenard_letraquenard_enseigner');
    }
}
