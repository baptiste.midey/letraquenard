<?php namespace Letraquenard\Letraquenard\Models;

use Model;

/**
 * Model
 */
class Utilisateur extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'letraquenard_letraquenard_utilisateur';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'promotion' => 'Letraquenard\Letraquenard\Models\Promotion'
    ];
    public $hasMany = [
        'repondre' => 'Letraquenard\Letraquenard\Models\Repondre'
    ];
}
