<?php namespace Letraquenard\Letraquenard\Models;

use Model;

/**
 * Model
 */
class Question extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'letraquenard_letraquenard_question';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsTo = [
        'matiere' => 'Letraquenard\Letraquenard\Models\Matiere'
    ];
    public $hasMany = [
        'repondre' => 'Letraquenard\Letraquenard\Models\Repondre'
    ];
}
