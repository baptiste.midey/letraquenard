<?php namespace Letraquenard\Letraquenard\Models;

use Model;

/**
 * Model
 */
class Matiere extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'letraquenard_letraquenard_matiere';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $hasMany = [
        'questions' => 'Letraquenard\Letraquenard\Models\Question',
        'enseigner' => 'Letraquenard\Letraquenard\Models\Enseigner'
    ];

    
    
}
