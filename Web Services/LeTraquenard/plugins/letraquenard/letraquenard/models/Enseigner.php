<?php namespace Letraquenard\Letraquenard\Models;

use Model;

/**
 * Model
 */
class Enseigner extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'letraquenard_letraquenard_enseigner';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'promotion' => 'Letraquenard\Letraquenard\Models\Promotion',
        'matiere' => 'Letraquenard\Letraquenard\Models\Matiere'
    ];
    
        
    

}
