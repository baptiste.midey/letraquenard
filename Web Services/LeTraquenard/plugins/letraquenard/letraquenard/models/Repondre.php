<?php namespace Letraquenard\Letraquenard\Models;

use Model;

/**
 * Model
 */
class Repondre extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'letraquenard_letraquenard_repondre';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'question' => 'Letraquenard\Letraquenard\Models\Question',
        'utilisateur' => 'Letraquenard\Letraquenard\Models\Utilisateur',
        'semestre' => 'Letraquenard\Letraquenard\Models\Semestre'
    ];
}
