<?php namespace LeTraquenard\ApiGenerator\Helpers;

Class Helpers {

	public function apiArrayResponseBuilder($statusCode = null, $message = null, $data = [])
	{
		$arr = [
			'status_code' => (isset($statusCode)) ? $statusCode : 500,
			'message' => (isset($message)) ? $message : 'error'
		];
		if (count($data) > 0) {
			$arr['data'] = $data;
		}

		return response()->json($arr, $arr['status_code']);
		//return $arr;
		
	}
	public function apiResponseBuilder($statusCode = null, $message = null, $data = null)
	{
		$arr = [
			'status_code' => (isset($statusCode)) ? $statusCode : 500,
			'message' => (isset($message)) ? $message : 'error',
			'data'=>(isset($date))?$data : 'error'
		];

		return response()->json($arr, $arr['status_code']);
		//return $arr;
		
	}
}