<?php namespace LeTraquenard\ApiGenerator\Controllers\API;

use Cms\Classes\Controller;
use BackendMenu;

use Illuminate\Http\Request;
use LeTraquenard\ApiGenerator\Helpers\Helpers;
use Illuminate\Support\Facades\Validator;
use Letraquenard\Letraquenard\Models\Question;
class QuestionController extends Controller
{
	protected $Question;

    protected $helpers;

    public function __construct(Question $Question, Helpers $helpers)
    {
        parent::__construct();
        $this->Question    = $Question;
        $this->helpers          = $helpers;
    }

    public function index(){

        $data = $this->Question->all()->toArray();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', $data);
    }

    public function show($id){

        $data = $this->Question::find($id);

        if ($data){
            return $this->helpers->apiResponseBuilder(200, 'success', $data);
        } else {
            $this->helpers->apiArrayResponseBuilder(404, 'not found', ['error' => 'Resource id=' . $id . ' could not be found']);
        }

    }

    public function store(Request $request){

    	$arr = $request->all();
        for ($i=0; $i < count($arr); $i++) { 
            $this->Question->{key($arr)} = current($arr);
            next($arr);
        }

        $validation = Validator::make($request->all(), $this->Question->rules);
        
        if( $validation->passes() ){
            $this->Question->save();
            return $this->helpers->apiArrayResponseBuilder(201, 'created', ['id' => $this->Question->id]);
        }else{
            return $this->helpers->apiArrayResponseBuilder(400, 'fail', $validation->errors() );
        }

    }

    public function update($id, Request $request){

        $this->Question = UtiliQuestionsateur::find($id);
    	$arr = $request->all();
        unset($arr['id']);

        for ($i=0; $i < count($arr); $i++) { 
            $this->Question->{key($arr)} = current($arr);
            next($arr);
        }

        $status = $this->Question->save();
    
        if( $status ){
            
            return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been updated successfully.');

        }else{

            return $this->helpers->apiArrayResponseBuilder(400, 'bad request', 'Error, data failed to update.');

        }
    }

    public function delete($id){

        $this->Question->where('id',$id)->delete();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been deleted successfully.');
    }

    public function destroy($id){

        $this->Question->where('id',$id)->delete();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been deleted successfully.');
    }


    public static function getAfterFilters() {return [];}
    public static function getBeforeFilters() {return [];}
    public static function getMiddleware() {return [];}
    public function callAction($method, $parameters=false) {
        return call_user_func_array(array($this, $method), $parameters);
    }
    
}