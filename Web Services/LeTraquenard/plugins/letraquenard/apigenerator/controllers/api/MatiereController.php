<?php namespace LeTraquenard\ApiGenerator\Controllers\API;

use Cms\Classes\Controller;
use BackendMenu;

use Illuminate\Http\Request;
use LeTraquenard\ApiGenerator\Helpers\Helpers;
use Illuminate\Support\Facades\Validator;
use Letraquenard\Letraquenard\Models\Matiere;
class MatiereController extends Controller
{
	protected $Matiere;

    protected $helpers;

    public function __construct(Matiere $Matiere, Helpers $helpers)
    {
        parent::__construct();
        $this->Matiere    = $Matiere;
        $this->helpers          = $helpers;
    }

    public function index(){

        $data = $this->Matiere->all()->toArray();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', $data);
    }

    public function show($id){

        $data = $this->Matiere::find($id);

        if ($data){
            return $this->helpers->apiResponseBuilder(200, 'success', $data);
        } else {
            $this->helpers->apiArrayResponseBuilder(404, 'not found', ['error' => 'Resource id=' . $id . ' could not be found']);
        }

    }

    public function store(Request $request){

    	$arr = $request->all();
        for ($i=0; $i < count($arr); $i++) { 
            $this->Matiere->{key($arr)} = current($arr);
            next($arr);
        }

        $validation = Validator::make($request->all(), $this->Matiere->rules);
        
        if( $validation->passes() ){
            $this->Matiere->save();
            return $this->helpers->apiArrayResponseBuilder(201, 'created', ['id' => $this->Matiere->id]);
        }else{
            return $this->helpers->apiArrayResponseBuilder(400, 'fail', $validation->errors() );
        }

    }

    public function update($id, Request $request){

        $this->Matiere = UtiliMatieresateur::find($id);
    	$arr = $request->all();
        unset($arr['id']);

        for ($i=0; $i < count($arr); $i++) { 
            $this->Matiere->{key($arr)} = current($arr);
            next($arr);
        }

        $status = $this->Matiere->save();
    
        if( $status ){
            
            return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been updated successfully.');

        }else{

            return $this->helpers->apiArrayResponseBuilder(400, 'bad request', 'Error, data failed to update.');

        }
    }

    public function delete($id){

        $this->Matiere->where('id',$id)->delete();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been deleted successfully.');
    }

    public function destroy($id){

        $this->Matiere->where('id',$id)->delete();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been deleted successfully.');
    }


    public static function getAfterFilters() {return [];}
    public static function getBeforeFilters() {return [];}
    public static function getMiddleware() {return [];}
    public function callAction($method, $parameters=false) {
        return call_user_func_array(array($this, $method), $parameters);
    }
    
}