<?php namespace LeTraquenard\ApiGenerator\Controllers\API;

use Cms\Classes\Controller;
use BackendMenu;

use Illuminate\Http\Request;
use LeTraquenard\ApiGenerator\Helpers\Helpers;
use Illuminate\Support\Facades\Validator;
use Letraquenard\Letraquenard\Models\Enseigner;
class EnseignerController extends Controller
{
	protected $Enseigner;

    protected $helpers;

    public function __construct(Enseigner $Enseigner, Helpers $helpers)
    {
        parent::__construct();
        $this->Enseigner    = $Enseigner;
        $this->helpers          = $helpers;
    }

    public function index(){

        $data = $this->Enseigner->all()->toArray();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', $data);
    }

    public function show($id){

        $data = $this->Enseigner::find($id);

        if ($data){
            return $this->helpers->apiResponseBuilder(200, 'success', $data);
        } else {
            $this->helpers->apiArrayResponseBuilder(404, 'not found', ['error' => 'Resource id=' . $id . ' could not be found']);
        }

    }

    public function store(Request $request){

    	$arr = $request->all();
        for ($i=0; $i < count($arr); $i++) { 
            $this->Enseigner->{key($arr)} = current($arr);
            next($arr);
        }

        $validation = Validator::make($request->all(), $this->Enseigner->rules);
        
        if( $validation->passes() ){
            $this->Enseigner->save();
            return $this->helpers->apiArrayResponseBuilder(201, 'created', ['id' => $this->Enseigner->id]);
        }else{
            return $this->helpers->apiArrayResponseBuilder(400, 'fail', $validation->errors() );
        }

    }

    public function update($id, Request $request){

        $this->Enseigner = UtiliEnseignersateur::find($id);
    	$arr = $request->all();
        unset($arr['id']);

        for ($i=0; $i < count($arr); $i++) { 
            $this->Enseigner->{key($arr)} = current($arr);
            next($arr);
        }

        $status = $this->Enseigner->save();
    
        if( $status ){
            
            return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been updated successfully.');

        }else{

            return $this->helpers->apiArrayResponseBuilder(400, 'bad request', 'Error, data failed to update.');

        }
    }

    public function delete($id){

        $this->Enseigner->where('id',$id)->delete();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been deleted successfully.');
    }

    public function destroy($id){

        $this->Enseigner->where('id',$id)->delete();

        return $this->helpers->apiArrayResponseBuilder(200, 'success', 'Data has been deleted successfully.');
    }


    public static function getAfterFilters() {return [];}
    public static function getBeforeFilters() {return [];}
    public static function getMiddleware() {return [];}
    public function callAction($method, $parameters=false) {
        return call_user_func_array(array($this, $method), $parameters);
    }
    
}