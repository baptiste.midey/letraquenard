<?php namespace LeTraquenard\ApiGenerator;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
	public $require = [
        'RainLab.Builder'
    ];
    
    public function registerComponents()
    {
        $this->app['Illuminate\Contracts\Http\Kernel']
        ->prependMiddleware('LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
    }

    public function registerSettings()
    {
    }
}
