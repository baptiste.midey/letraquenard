<?php

Route::post('fatoni/generate/api', array('as' => 'fatoni.generate.api', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\ApiGeneratorController@generateApi'));
Route::post('fatoni/update/api/{id}', array('as' => 'fatoni.update.api', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\ApiGeneratorController@updateApi'));
Route::get('fatoni/delete/api/{id}', array('as' => 'fatoni.delete.api', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\ApiGeneratorController@deleteApi'));

Route::resource('api/enseigner', 'LeTraquenard\ApiGenerator\Controllers\API\EnseignerController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/enseigner/{id}/delete', ['as' => 'api/enseigner.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\EnseignerController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::resource('api/matiere', 'LeTraquenard\ApiGenerator\Controllers\API\MatiereController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/matiere/{id}/delete', ['as' => 'api/matiere.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\MatiereController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::resource('api/promotion', 'LeTraquenard\ApiGenerator\Controllers\API\PromotionController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/promotion/{id}/delete', ['as' => 'api/promotion.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\PromotionController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::resource('api/question', 'LeTraquenard\ApiGenerator\Controllers\API\QuestionController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/question/{id}/delete', ['as' => 'api/question.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\QuestionController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::resource('api/repondre', 'LeTraquenard\ApiGenerator\Controllers\API\RepondreController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/repondre/{id}/delete', ['as' => 'api/repondre.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\RepondreController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::resource('api/semestre', 'LeTraquenard\ApiGenerator\Controllers\API\SemestreController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/semestre/{id}/delete', ['as' => 'api/semestre.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\SemestreController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::resource('api/utilisateur', 'LeTraquenard\ApiGenerator\Controllers\API\UilisateurController', ['except' => ['destroy', 'create', 'edit']])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');
Route::get('api/utilisateur/{id}/delete', ['as' => 'api/utilisateur.delete', 'uses' => 'LeTraquenard\ApiGenerator\Controllers\API\UilisateurController@destroy'])->middleware('\LeTraquenard\ApiGenerator\Middleware\LeTraquenardToken');