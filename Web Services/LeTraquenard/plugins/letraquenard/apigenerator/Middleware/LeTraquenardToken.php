<?php namespace LeTraquenard\ApiGenerator\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Letraquenard\Letraquenard\Models\Utilisateur;
use App\Http\Middleware;
use Exception;

class LeTraquenardToken extends \Backend\Classes\Controller
{
    protected $app;

    public function __construct()
    {
        parent::__construct();
    }
    public function handle($request, Closure $response)
    {
        if($request->hasHeader('email') && $request->hasHeader('mdp')){
            try{
                $mail = $request->header('email');
                $mdp = $request->header('mdp');
                $utilisateur = Utilisateur::where('mail', $mail)->get();
                if(!isset($utilisateur[0])) return response()->json(['error' => 'Unauthenticated.'], 401);
 
                if($utilisateur[0]->mdp == $mdp){
                    return $response($request);
                }
            }
            catch( \Symfony\Component\Debug\Exception\FatalThrowableError $e){
                throw $e;
            }
        }
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }
}