<?php namespace LeTraquenard\ApiGenerator\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLeTraquenardApigeneratorData extends Migration
{
    public function up()
    {
        Schema::create('LeTraquenard_apigenerator_data', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('endpoint');
            $table->string('model');
            $table->string('description')->nullable();
            $table->text('custom_format')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('LeTraquenard_apigenerator_data');
    }
}
