<?php return array (
  'october\\demo\\Plugin' => 'plugins\\october\\demo\\Plugin.php',
  'backend\\Controllers\\index' => 'modules\\backend\\controllers\\index.php',
  'backend\\Controllers\\auth' => 'modules\\backend\\controllers\\auth.php',
  'system\\Controllers\\updates' => 'modules\\system\\controllers\\updates.php',
  'ahmadfatoni\\apigenerator\\Plugin' => 'plugins\\ahmadfatoni\\apigenerator\\Plugin.php',
  'rainlab\\builder\\Plugin' => 'plugins\\rainlab\\builder\\Plugin.php',
  'backend\\Controllers\\users' => 'modules\\backend\\controllers\\users.php',
  'ahmadfatoni\\apigenerator\\Controllers\\apigeneratorcontroller' => 'plugins\\ahmadfatoni\\apigenerator\\controllers\\apigeneratorcontroller.php',
  'AhmadFatoni\\ApiGenerator\\Models\\ApiGenerator' => 'plugins\\ahmadfatoni\\apigenerator\\models\\ApiGenerator.php',
  'RainLab\\Builder\\Classes\\ComponentHelper' => 'plugins\\rainlab\\builder\\classes\\ComponentHelper.php',
  'RainLab\\Builder\\Classes\\BaseModel' => 'plugins\\rainlab\\builder\\classes\\BaseModel.php',
  'RainLab\\Builder\\Classes\\YamlModel' => 'plugins\\rainlab\\builder\\classes\\YamlModel.php',
  'RainLab\\Builder\\Classes\\PluginYamlModel' => 'plugins\\rainlab\\builder\\classes\\PluginYamlModel.php',
  'RainLab\\Builder\\Classes\\PluginBaseModel' => 'plugins\\rainlab\\builder\\classes\\PluginBaseModel.php',
  'RainLab\\Builder\\Classes\\PluginCode' => 'plugins\\rainlab\\builder\\classes\\PluginCode.php',
  'RainLab\\Builder\\Classes\\ModelModel' => 'plugins\\rainlab\\builder\\classes\\ModelModel.php',
  'RainLab\\Builder\\Classes\\ModelFileParser' => 'plugins\\rainlab\\builder\\classes\\ModelFileParser.php',
  'RainLab\\Builder\\Classes\\PhpSourceStream' => 'plugins\\rainlab\\builder\\classes\\PhpSourceStream.php',
  'rainlab\\builder\\Controllers\\index' => 'plugins\\rainlab\\builder\\controllers\\index.php',
  'RainLab\\Builder\\Classes\\IndexOperationsBehaviorBase' => 'plugins\\rainlab\\builder\\classes\\IndexOperationsBehaviorBase.php',
  'RainLab\\Builder\\Behaviors\\IndexPluginOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexPluginOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexDatabaseTableOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexDatabaseTableOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexModelOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexModelOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexModelFormOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexModelFormOperations.php',
  'RainLab\\Builder\\FormWidgets\\FormBuilder' => 'plugins\\rainlab\\builder\\formwidgets\\FormBuilder.php',
  'RainLab\\Builder\\Behaviors\\IndexModelListOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexModelListOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexPermissionsOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexPermissionsOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexMenusOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexMenusOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexVersionsOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexVersionsOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexLocalizationOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexLocalizationOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexControllerOperations' => 'plugins\\rainlab\\builder\\behaviors\\IndexControllerOperations.php',
  'RainLab\\Builder\\Behaviors\\IndexDataRegistry' => 'plugins\\rainlab\\builder\\behaviors\\IndexDataRegistry.php',
  'RainLab\\Builder\\Widgets\\PluginList' => 'plugins\\rainlab\\builder\\widgets\\PluginList.php',
  'RainLab\\Builder\\Widgets\\DatabaseTableList' => 'plugins\\rainlab\\builder\\widgets\\DatabaseTableList.php',
  'RainLab\\Builder\\Widgets\\ModelList' => 'plugins\\rainlab\\builder\\widgets\\ModelList.php',
  'RainLab\\Builder\\Widgets\\VersionList' => 'plugins\\rainlab\\builder\\widgets\\VersionList.php',
  'RainLab\\Builder\\Widgets\\LanguageList' => 'plugins\\rainlab\\builder\\widgets\\LanguageList.php',
  'RainLab\\Builder\\Widgets\\ControllerList' => 'plugins\\rainlab\\builder\\widgets\\ControllerList.php',
  'RainLab\\Builder\\Models\\Settings' => 'plugins\\rainlab\\builder\\models\\Settings.php',
  'RainLab\\Builder\\Classes\\IconList' => 'plugins\\rainlab\\builder\\classes\\IconList.php',
  'RainLab\\Builder\\Rules\\Reserved' => 'plugins\\rainlab\\builder\\rules\\Reserved.php',
  'RainLab\\Builder\\Classes\\LocalizationModel' => 'plugins\\rainlab\\builder\\classes\\LocalizationModel.php',
  'RainLab\\Builder\\Classes\\FilesystemGenerator' => 'plugins\\rainlab\\builder\\classes\\FilesystemGenerator.php',
  'letraquenard\\letraquenard\\Plugin' => 'plugins\\letraquenard\\letraquenard\\Plugin.php',
  'RainLab\\Builder\\Classes\\PluginVector' => 'plugins\\rainlab\\builder\\classes\\PluginVector.php',
  'RainLab\\Builder\\Classes\\DatabaseTableModel' => 'plugins\\rainlab\\builder\\classes\\DatabaseTableModel.php',
  'RainLab\\Builder\\Classes\\EnumDbType' => 'plugins\\rainlab\\builder\\classes\\EnumDbType.php',
  'RainLab\\Builder\\Classes\\PluginVersion' => 'plugins\\rainlab\\builder\\classes\\PluginVersion.php',
  'RainLab\\Builder\\Classes\\ControllerModel' => 'plugins\\rainlab\\builder\\classes\\ControllerModel.php',
  'RainLab\\Builder\\Classes\\MigrationColumnType' => 'plugins\\rainlab\\builder\\classes\\MigrationColumnType.php',
  'RainLab\\Builder\\Classes\\DatabaseTableSchemaCreator' => 'plugins\\rainlab\\builder\\classes\\DatabaseTableSchemaCreator.php',
  'RainLab\\Builder\\Classes\\TableMigrationCodeGenerator' => 'plugins\\rainlab\\builder\\classes\\TableMigrationCodeGenerator.php',
  'RainLab\\Builder\\Classes\\MigrationModel' => 'plugins\\rainlab\\builder\\classes\\MigrationModel.php',
  'RainLab\\Builder\\Classes\\ModelYamlModel' => 'plugins\\rainlab\\builder\\classes\\ModelYamlModel.php',
  'RainLab\\Builder\\Classes\\ModelFormModel' => 'plugins\\rainlab\\builder\\classes\\ModelFormModel.php',
  'RainLab\\Builder\\Classes\\ModelListModel' => 'plugins\\rainlab\\builder\\classes\\ModelListModel.php',
  'system\\Controllers\\settings' => 'modules\\system\\controllers\\settings.php',
  'cms\\Controllers\\themes' => 'modules\\cms\\controllers\\themes.php',
  'cms\\Controllers\\index' => 'modules\\cms\\controllers\\index.php',
  'RainLab\\Builder\\Components\\RecordList' => 'plugins\\rainlab\\builder\\components\\RecordList.php',
  'RainLab\\Builder\\Components\\RecordDetails' => 'plugins\\rainlab\\builder\\components\\RecordDetails.php',
  'RainLab\\Builder\\Classes\\MenusModel' => 'plugins\\rainlab\\builder\\classes\\MenusModel.php',
  'RainLab\\Builder\\Classes\\ControllerBehaviorLibrary' => 'plugins\\rainlab\\builder\\classes\\ControllerBehaviorLibrary.php',
  'RainLab\\Builder\\Classes\\StandardBehaviorsRegistry' => 'plugins\\rainlab\\builder\\classes\\StandardBehaviorsRegistry.php',
  'RainLab\\Builder\\Classes\\PermissionsModel' => 'plugins\\rainlab\\builder\\classes\\PermissionsModel.php',
  'AhmadFatoni\\ApiGenerator\\Controllers\\ApiGeneratorController' => 'plugins\\ahmadfatoni\\apigenerator\\controllers\\ApiGeneratorController.php',
  'AhmadFatoni\\ApiGenerator\\Controllers\\API\\LeTraquenardController' => 'plugins\\ahmadfatoni\\apigenerator\\controllers\\api\\LeTraquenardController.php',
  'Letraquenard\\Letraquenard\\Models\\Enseigner' => 'plugins\\letraquenard\\letraquenard\\models\\Enseigner.php',
  'AhmadFatoni\\ApiGenerator\\Helpers\\Helpers' => 'plugins\\ahmadfatoni\\apigenerator\\helpers\\Helpers.php',
  'Letraquenard\\Letraquenard\\Models\\User' => 'plugins\\letraquenard\\letraquenard\\models\\User.php',
  'Letraquenard\\Letraquenard\\Models\\Utilisateur' => 'plugins\\letraquenard\\letraquenard\\models\\Utilisateur.php',
  'RainLab\\Builder\\Classes\\LanguageMixer' => 'plugins\\rainlab\\builder\\classes\\LanguageMixer.php',
  'RainLab\\Builder\\Classes\\MigrationFileParser' => 'plugins\\rainlab\\builder\\classes\\MigrationFileParser.php',
  'AhmadFatoni\\ApiGenerator\\Controllers\\API\\UtilisateursController' => 'plugins\\ahmadfatoni\\apigenerator\\controllers\\api\\UtilisateursController.php',
  'AhmadFatoni\\ApiGenerator\\Controllers\\API\\PromotionsController' => 'plugins\\ahmadfatoni\\apigenerator\\controllers\\api\\PromotionsController.php',
  'Letraquenard\\Letraquenard\\Models\\Promotion' => 'plugins\\letraquenard\\letraquenard\\models\\Promotion.php',
  'mohsin\\auth\\Plugin' => 'plugins\\mohsin\\auth\\Plugin.php',
  'mohsin\\rest\\Plugin' => 'plugins\\mohsin\\rest\\Plugin.php',
  'Mohsin\\Rest\\Classes\\ApiManager' => 'plugins\\mohsin\\rest\\classes\\ApiManager.php',
  'Mohsin\\Rest\\Models\\Setting' => 'plugins\\mohsin\\rest\\models\\Setting.php',
  'Mohsin\\Rest\\Controllers\\Settings' => 'plugins\\mohsin\\rest\\controllers\\Settings.php',
  'Mohsin\\Rest\\Models\\Node' => 'plugins\\mohsin\\rest\\models\\Node.php',
  'Mohsin\\Auth\\Classes\\AuthManager' => 'plugins\\mohsin\\auth\\classes\\AuthManager.php',
  'Mohsin\\Rest\\Console\\CreateRestController' => 'plugins\\mohsin\\rest\\console\\CreateRestController.php',
  'mohsin\\oauth2\\Plugin' => 'plugins\\mohsin\\oauth2\\Plugin.php',
  'mohsin\\rest\\Controllers\\settings' => 'plugins\\mohsin\\rest\\controllers\\settings.php',
  'Mohsin\\OAuth2\\Classes\\RouteRegistrar' => 'plugins\\mohsin\\oauth2\\classes\\RouteRegistrar.php',
  'Mohsin\\OAuth2\\Behaviors\\ApiTokenable' => 'plugins\\mohsin\\oauth2\\behaviors\\ApiTokenable.php',
  'lovata\\shopaholic\\Plugin' => 'plugins\\lovata\\shopaholic\\Plugin.php',
  'lovata\\toolbox\\Plugin' => 'plugins\\lovata\\toolbox\\Plugin.php',
  'Lovata\\Toolbox\\Models\\CommonSettings' => 'plugins\\lovata\\toolbox\\models\\CommonSettings.php',
  'Lovata\\Toolbox\\Models\\Settings' => 'plugins\\lovata\\toolbox\\models\\Settings.php',
  'Lovata\\Shopaholic\\Models\\Settings' => 'plugins\\lovata\\shopaholic\\models\\Settings.php',
  'Lovata\\Toolbox\\Traits\\Helpers\\TraitCached' => 'plugins\\lovata\\toolbox\\traits\\helpers\\TraitCached.php',
  'Lovata\\Shopaholic\\Models\\Currency' => 'plugins\\lovata\\shopaholic\\models\\Currency.php',
  'Lovata\\Toolbox\\Classes\\Helper\\PriceHelper' => 'plugins\\lovata\\toolbox\\classes\\helper\\PriceHelper.php',
  'Lovata\\Toolbox\\Traits\\Helpers\\PriceHelperTrait' => 'plugins\\lovata\\toolbox\\traits\\helpers\\PriceHelperTrait.php',
  'Lovata\\Shopaholic\\Models\\Offer' => 'plugins\\lovata\\shopaholic\\models\\Offer.php',
  'AhmadFatoni\\ApiGenerator\\Controllers\\API\\UtilisateurController' => 'plugins\\ahmadfatoni\\apigenerator\\controllers\\api\\UtilisateurController.php',
  'RainLab\\Builder\\FormWidgets\\MenuEditor' => 'plugins\\rainlab\\builder\\formwidgets\\MenuEditor.php',
  'letraquenard\\apigenerator\\Plugin' => 'plugins\\letraquenard\\apigenerator\\Plugin.php',
  'LeTraquenard\\ApiGenerator\\Controllers\\API\\UtilisateurController' => 'plugins\\letraquenard\\apigenerator\\controllers\\api\\UtilisateurController.php',
  'LeTraquenard\\ApiGenerator\\Helpers\\Helpers' => 'plugins\\letraquenard\\apigenerator\\helpers\\Helpers.php',
  'LeTraquenard\\apigenerator\\Controllers\\apigeneratorcontroller' => 'plugins\\letraquenard\\apigenerator\\controllers\\apigeneratorcontroller.php',
  'LeTraquenard\\ApiGenerator\\Models\\ApiGenerator' => 'plugins\\letraquenard\\apigenerator\\models\\ApiGenerator.php',
  'backend\\Controllers\\media' => 'modules\\backend\\controllers\\media.php',
  'LeTraquenard\\ApiGenerator\\Controllers\\API\\PromotionController' => 'plugins\\letraquenard\\apigenerator\\controllers\\api\\PromotionController.php',
  'LeTraquenard\\ApiGenerator\\Middleware\\LeTraquenardToken' => 'plugins\\letraquenard\\apigenerator\\middleware\\LeTraquenardToken.php',
  'LeTraquenard\\ApiGenerator\\Controllers\\ApiGeneratorController' => 'plugins\\letraquenard\\apigenerator\\controllers\\ApiGeneratorController.php',
);