package com.example.letraquenard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.letraquenard.util.Files.CsvReader;
import com.example.letraquenard.util.Session;
import com.example.letraquenard.util.webservices.WebServices;

public class AdminAccueil extends AppCompatActivity  {
    CsvReader csv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin);
    }
    public void Clickinterroger(View view){
        Intent i = new Intent(getApplicationContext(), Interrogation.class);
        startActivity(i);
        //new JsonTask().execute();
    }
    public void Clickquestions(View view){
        Intent i = new Intent(getApplicationContext(), Questions.class);
        startActivity(i);
        //new JsonTask().execute();
    }
    public void Clickimport(View view){
        Intent i = new Intent(getApplicationContext(), Imports.class);
        startActivity(i);
    }

    public void VidageTableBdd(View view) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Vider la base de donnée")
                .setMessage("Êtes-vous sûr de vouloir vider la base de données")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Thread vidageBdd = new Thread(()-> {
                            try {
                                WebServices.DeleteAll();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        vidageBdd.start();
                        try {
                            vidageBdd.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                })
                .setNegativeButton("Non", null)
                .show();
    }

    public void DeconnexionAdmin(View view){
        Session.user = null;
        this.finish();
    }
}
