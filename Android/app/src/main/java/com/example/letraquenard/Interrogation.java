package com.example.letraquenard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.letraquenard.Objects.Enseigner;
import com.example.letraquenard.Objects.Matiere;
import com.example.letraquenard.Objects.Promotion;
import com.example.letraquenard.Objects.Question;
import com.example.letraquenard.Objects.Repondre;
import com.example.letraquenard.Objects.Semestre;
import com.example.letraquenard.Objects.Utilisateur;
import com.example.letraquenard.util.RandomTraquenard;
import com.example.letraquenard.util.Views.Adapter.AnneeSpinner;
import com.example.letraquenard.util.Views.Adapter.MatiereSpinner;
import com.example.letraquenard.util.Views.Adapter.PromotionSpinner;
import com.example.letraquenard.util.webservices.WebServices;

import java.util.List;

public class Interrogation extends AppCompatActivity {
    private List<Promotion> promotions;
    private List<Matiere> matieres;
    private Spinner spinner_promotion;
    private Spinner spinner_matiere;
    private Spinner anneespinner;
    private TextView txt_eleve;
    private Context context;
    private MatiereSpinner adapter;
    private ProgressDialog prog;
    private Activity activite;
    private RandomTraquenard rm;
    private EditText question_txt;
    private Question question;
    private Utilisateur eleve;
    private Repondre repondre;
    private List<Semestre> semestres;
    private Semestre semestre;
    private boolean reponse;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interrogation);
        spinner_promotion = (Spinner) this.findViewById(R.id.spinner);
        spinner_matiere = (Spinner) this.findViewById(R.id.spinner2);
        anneespinner = (Spinner) this.findViewById(R.id.anneespinner);
        question_txt = this.findViewById(R.id.editTextTextPersonName);
        txt_eleve = this.findViewById(R.id.textViewEleve);
        spinner_matiere.setEnabled(false);
        context = this;
        activite = this;
        rm = new RandomTraquenard();
        Thread listeSemestres = new Thread(()-> {
            try {
                semestres = WebServices.List(Semestre.class);
                semestres.add(new Semestre(0,"",""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        listeSemestres.start();
        try {
            listeSemestres.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AnneeSpinner adapter_annee = new AnneeSpinner(this,R.layout.spinner_annee,R.id.textViewAnnee,semestres);
        this.anneespinner.setAdapter(adapter_annee);
        this.anneespinner.setSelection(semestres.size()-1);
        anneespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if ( id != 0) {
                   semestre = (Semestre) anneespinner.getSelectedItem();
                   anneespinner.getSelectedItem().toString();
                }
                else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        Thread listePromotions = new Thread(()-> {
            try {
                promotions = WebServices.List(Promotion.class);
                promotions.add(new Promotion());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        listePromotions.start();
        try {
            listePromotions.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PromotionSpinner adapter_promotion = new PromotionSpinner(this,R.layout.spinner_promotion,R.id.textViewPromotion,promotions);
        this.spinner_promotion.setAdapter(adapter_promotion);
        this.spinner_promotion.setSelection(promotions.size()-1);
        spinner_promotion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if ( id != 0) {
                    Promotion p = (Promotion) spinner_promotion.getSelectedItem();
                    Thread matiereParPromotion = new Thread(()-> {
                        try {
                            matieres = Enseigner.matiere_promotions((int)p.getId());
                            matieres.add(new Matiere());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    matiereParPromotion.start();
                    try {
                        matiereParPromotion.join();
                        spinner_matiere.setEnabled(true);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    MatiereSpinner adpater_matiere = new MatiereSpinner(activite,R.layout.spinner_matiere,R.id.textViewMatiere,matieres);
                    spinner_matiere.setAdapter(adpater_matiere);
                    spinner_matiere.setSelection(matieres.size()-1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        spinner_matiere.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (id != 0 )
                {
                    Promotion p = (Promotion) spinner_promotion.getSelectedItem();
                    Matiere m = (Matiere) spinner_matiere.getSelectedItem();
                    if (m.getId() == 0 && p.getId() == 0 ) {
                        question_txt.setText("");
                        txt_eleve.setText("");
                    }
                    else {
                        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
                        Thread randomEleveQuestion = new Thread(()-> {
                            try {
                                eleve = rm.utilisateurAlea((int)p.getId());
                                question = rm.questionAlea((int)m.getId());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        randomEleveQuestion.start();
                        try {
                            randomEleveQuestion.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        eleve = rm.utilisateurAlea((int)p.getId());
                        if (question.getId() == 0 && p.getId() == 0) {
                            question_txt.setText("");
                            txt_eleve.setText("");
                        } else {
                            question_txt.setText(question.getLibelle());
                            txt_eleve.setText(eleve.getPrenom()+" "+eleve.getNom());
                        }
                    }
                } else {
                    //
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
                question_txt.setText("");
                txt_eleve.setText("");
            }

        });
    }
    public void Bonnereponse(View view) {
        if (semestre != null) {
            Thread bonneReponse = new Thread(()-> {
                try {
                    reponse = true;
                    repondre = new Repondre(reponse, question.getId(), semestre.getId(), eleve.getId());
                    WebServices.Create(repondre);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            bonneReponse.start();
            try {
                bonneReponse.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Reset();
        }
        else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Erreur dans la sélection")
                    .setMessage("Veuillez sélectionner un semestre")
                    .show();
        }
    }
    public void Mauvaisereponse(View view) {
        if (semestre != null) {
            Thread mauvaiseReponse = new Thread(()-> {
                try {
                    reponse = false;
                    repondre = new Repondre(reponse, question.getId(), 1, eleve.getId());
                    WebServices.Create(repondre);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            mauvaiseReponse.start();
            try {
                mauvaiseReponse.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Reset();
        }
        else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Erreur dans la sélection")
                    .setMessage("Veuillez sélectionner un semestre")
                    .show();
        }
    }
    public void Reset() {
        Matiere m = (Matiere) spinner_matiere.getSelectedItem();
        Promotion p = (Promotion) spinner_promotion.getSelectedItem();
        if (m.getId() == 0 && p.getId() == 0) {
            question_txt.setText("");
            txt_eleve.setText("");
        }
        else {
            Thread randomEleveQuestionReset = new Thread(()-> {
                try {
                    question = rm.questionAlea((int)m.getId());
                    eleve = rm.utilisateurAlea((int)p.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            randomEleveQuestionReset.start();
            try {
                randomEleveQuestionReset.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (question.getId() == 0 && p.getId() == 0) {
                question_txt.setText("");
                txt_eleve.setText("");
            } else {
                question_txt.setText(question.getLibelle());
                txt_eleve.setText(eleve.getPrenom()+""+eleve.getNom());
            }
        }
    }
    public void Clickretour(View view) {
        this.finish();
    }
}