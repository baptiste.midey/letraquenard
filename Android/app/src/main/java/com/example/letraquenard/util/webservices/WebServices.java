package com.example.letraquenard.util.webservices;

import com.example.letraquenard.Objects.Enseigner;
import com.example.letraquenard.Objects.LeTraquenardObject;
import com.example.letraquenard.Objects.Matiere;
import com.example.letraquenard.Objects.Promotion;
import com.example.letraquenard.Objects.Question;
import com.example.letraquenard.Objects.Repondre;
import com.example.letraquenard.Objects.Semestre;
import com.example.letraquenard.Objects.Utilisateur;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WebServices {
    public static String Mdp;
    public static String Email;
    protected   static  String BaseUrl = "http://toto.reseau-labo.fr/LeTraquenard/api/";
    //private static  String BaseUrl = "http://192.168.14.251/LeTraquenard/api/";
    private static ObjectMapper mapper = new ObjectMapper();

    public static <T extends LeTraquenardObject> List<T> List(Class<T> returnClass) throws Exception {
        URL url = new URL(BaseUrl + returnClass.getSimpleName().toLowerCase());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("GET");
        conn.connect();

        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 200) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            JSONParser parse = new JSONParser();
            JSONObject data_obj = (JSONObject) parse.parse(content.toString());

            List<T> lst = new ArrayList<>();
            for (Object jsObject : ((JSONArray)data_obj.get("data"))) {
                lst.add(mapper.readValue(((JSONObject)jsObject).toString(),returnClass));
            }
            return lst;
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }

    public static <T extends LeTraquenardObject> T Get(Class<T> returnClass, int id) throws Exception {
        URL url = new URL(BaseUrl + returnClass.getSimpleName().toLowerCase() + "/" + id);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("GET");
        conn.connect();

        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 200) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            JSONParser parse = new JSONParser();
            JSONObject data_obj = (JSONObject) parse.parse(content.toString());

            return mapper.readValue(data_obj.get("data").toString(), returnClass);
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }

    public static <T extends LeTraquenardObject> void Delete(Class<T> returnClass, int id) throws Exception {
        URL url = new URL(BaseUrl + returnClass.getSimpleName().toLowerCase() + "/" + id + "/delete");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("GET");
        conn.connect();

        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 200) {
            return;
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }

    public static <T extends LeTraquenardObject> void DeleteAll() throws Exception {
        URL url = new URL(BaseUrl + "/delete");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("GET");
        conn.connect();

        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 200) {
            return;
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }

    public static <T extends LeTraquenardObject> int Create(T object) throws Exception {
        String strUrl = BaseUrl + object.getClass().getSimpleName().toLowerCase();
        URL url = new URL( strUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json; utf-8");
        conn.setDoOutput(true);
        conn.connect();

        String jsonInputString = mapper.writeValueAsString(object);
        OutputStream os = conn.getOutputStream();
        byte[] input = jsonInputString.getBytes();
        os.write(input);

        os.flush();
        os.close();
        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 201) {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            JSONParser parse = new JSONParser();
            JSONObject data_obj = (JSONObject) parse.parse(content.toString());
            JSONObject data = (JSONObject) data_obj.get("data");
            return mapper.readValue(data.get("id").toString(), new TypeReference<Integer>(){});
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }

    public static <T extends LeTraquenardObject> void Update(T object, int id) throws Exception {
        String strUrl = BaseUrl + object.getClass().getSimpleName().toLowerCase() +"/" + id;
        URL url = new URL( strUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("PUT");
        conn.setRequestProperty("Content-Type", "application/json; utf-8");
        conn.setDoOutput(true);
        conn.connect();

        String jsonInputString = mapper.writeValueAsString(object);
        OutputStream os = conn.getOutputStream();
        byte[] input = jsonInputString.getBytes();
        os.write(input);

        os.flush();
        os.close();

        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 200) {
            return;
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else if (responsecode == 400) {
            throw new RuntimeException("bad request, Error, data failed to update.");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }

    public static <T extends LeTraquenardObject> Utilisateur Login() throws Exception {
        URL url = new URL(BaseUrl + "login");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("email",Email);
        conn.setRequestProperty("mdp",Mdp);
        conn.setRequestMethod("GET");
        conn.connect();

        //Getting the response code
        int responsecode = conn.getResponseCode();

        if (responsecode == 200) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                JSONParser parse = new JSONParser();
                JSONObject data_obj = (JSONObject) parse.parse(content.toString());

                return mapper.readValue(data_obj.get("data").toString(), Utilisateur.class);
        }
        else if (responsecode == 401) {
            throw new RuntimeException("Unauthorized");
        }
        else{
            throw new RuntimeException("HttpResponseCode: " + responsecode);
        }
    }
}
