package com.example.letraquenard.Objects;

import com.example.letraquenard.util.Security.Security;
import com.example.letraquenard.util.webservices.WebServices;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Utilisateur extends LeTraquenardObject {
    private int id;
    private String nom;
    private String prenom;
    private String mail;
    private String mdp;
    private  Boolean isInterogate;
    private int promotion_id;

    public  Utilisateur(){}



    public Utilisateur(int id, String nom, String prenom, String mail, String mdp) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.mdp = Security.Encrypt(mdp);
    }

    public Utilisateur(int id, String nom, String prenom, String mail, String mdp, Boolean isInterogate, int promotion_id) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.mdp = mdp;
        this.isInterogate = false;
        this.promotion_id = promotion_id;
    }
/*public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }*/

    public int getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(int promotion_id) {
        this.promotion_id = promotion_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = Security.Encrypt( mdp);
    }

    public Boolean getInterogate() {
        return isInterogate;
    }

    public void setInterogate(Boolean interogate) {
        isInterogate = interogate;
    }

    public String moyenneParSemestre(int id, int semestre_id) {
        String res = "Votre moyenne est de ";
        List<Repondre> total_reponses = new ArrayList<>();
        List<Repondre> reponsesparsemestre = new ArrayList<>();
        try {
            List<Repondre> reponses = WebServices.List(Repondre.class);
            Predicate<Repondre> rpred = r->r.getUtilisateur_id() == id;
            total_reponses = reponses.stream().filter(rpred).collect(Collectors.toList());
            //for(int i =0;i<total_reponses.size();i++){
                Predicate<Repondre> smpred = r->r.getSemestre_id() == semestre_id;
                reponsesparsemestre.addAll(total_reponses.stream().filter(smpred).collect(Collectors.toList()));
            //}
            Predicate<Repondre> vf = r->r.isVraifaux() == true;
            res+= reponsesparsemestre.stream().filter(vf).collect(Collectors.toList()).size();
            res+= "/"+reponsesparsemestre.size()+" sur ce semestre";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
