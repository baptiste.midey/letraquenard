package com.example.letraquenard.util.Views.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.letraquenard.Objects.Matiere;

import java.util.List;

public class MatiereSpinner extends BaseAdapter {
    private List<Matiere> matieres;
    private int listItemLayoutResource;
    private int textViewMatiere;
    private LayoutInflater flater;

    public MatiereSpinner(Activity context, int listItemLayoutResource, int textViewMatiere, List<Matiere> matieres) {
        this.listItemLayoutResource = listItemLayoutResource;
        this.textViewMatiere = textViewMatiere;
        this.matieres = matieres;
        this.flater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        if (this.matieres == null) {
            return 0;
        }
        return this.matieres.size();
    }

    @Override
    public Object getItem(int position) {
        return this.matieres.get(position);
    }

    @Override
    public long getItemId(int position) {
        Matiere m = (Matiere) this.getItem(position);
        return m.getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Matiere m = (Matiere) getItem(position);
        View rowView = this.flater.inflate(this.listItemLayoutResource,null,true);
        TextView textViewMatiere = (TextView) rowView.findViewById(this.textViewMatiere);
        textViewMatiere.setText(m.getLibelle());
        return rowView;
    }
}