package com.example.letraquenard.Objects;

import java.io.Serializable;

public class Question extends LeTraquenardObject implements Serializable {
    int id;
    String libelle;
    int matiere_id;


    public Question(){}

    public Question(int id, String libelle,int matiere_id) {
        this.id = id;
        this.libelle = libelle;
        this.matiere_id = matiere_id;
    }

    public int getId() {
        return id;
    }

    public void setId_Question(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getMatiere_id() {
        return matiere_id;
    }

    public void setMatiere_id (int matiere_id) {
        this.matiere_id= matiere_id;
    }
}
