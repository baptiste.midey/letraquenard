package com.example.letraquenard.Objects;

import com.example.letraquenard.util.webservices.WebServices;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Enseigner extends LeTraquenardObject {
     int id;
     int promotion_id;
     int matiere_id;


     public Enseigner(){}

    public Enseigner(int id,int promotion_id, int matiere_id) {
         this.id = id;
        this.promotion_id = promotion_id;
        this.matiere_id = matiere_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(int promotion_id) {
        this.promotion_id = promotion_id;
    }

    public int getMatiere_id() {
        return matiere_id;
    }

    public void setMatiere_id(int matiere_id) {
        this.matiere_id = matiere_id;
    }

    public static List<Matiere> matiere_promotions(int promotion_id) {
         List<Matiere> matieres = new ArrayList<>();
        try {
            List<Enseigner> enseigners = WebServices.List(Enseigner.class);
            Predicate<Enseigner> pred = e->e.getPromotion_id() == promotion_id;
            List<Enseigner> enseigner_list = enseigners.stream().filter(pred).collect(Collectors.toList());
            for ( Enseigner e : enseigner_list) {
                matieres.add(WebServices.Get(Matiere.class,e.getMatiere_id()));
            }
        }catch (Exception e) {
           e.printStackTrace();
        }
        return matieres;
    }
}
