package com.example.letraquenard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.letraquenard.Objects.Promotion;
import com.example.letraquenard.Objects.Semestre;
import com.example.letraquenard.util.Session;
import com.example.letraquenard.util.Views.Adapter.AnneeSpinner;
import com.example.letraquenard.util.webservices.WebServices;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EleveAccueil extends AppCompatActivity {
    private Spinner anneespinner;
    private Activity activite;
    private Context context;
    private List<Semestre> semestres;
    private TextView txtnom;
    private TextView txtprenom;
    private TextView txtpromotion;
    private List<Promotion> promotions;
    private List<Promotion> promotionuser;
    private ProgressBar pgb;
    private String moyenne;
    private TextView txtmoyenne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moyenne);
        anneespinner = (Spinner) this.findViewById(R.id.anneespinner);
        txtnom = this.findViewById(R.id.txtnom);
        txtprenom = this.findViewById(R.id.txtprenom);
        txtpromotion = this.findViewById(R.id.txtpromotion);
        txtmoyenne = this.findViewById(R.id.txtmoyenne);
        pgb = this.findViewById(R.id.prog4);
        activite = this;
        context = this;
        txtnom.setText(Session.user.getNom());
        txtprenom.setText(Session.user.getPrenom());
        txtmoyenne.setText("");
        Thread listeSemestres = new Thread(()-> {
            try {
                promotionuser = new ArrayList<>();
                promotions = WebServices.List(Promotion.class);
                Predicate<Promotion> ppred = p->p.getId() == Session.user.getPromotion_id();
                promotionuser = promotions.stream().filter(ppred).collect(Collectors.toList());
                semestres = WebServices.List(Semestre.class);
                semestres.add(new Semestre(0,"",""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        listeSemestres.start();
        try {
            listeSemestres.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        AnneeSpinner adapter_annee = new AnneeSpinner(this,R.layout.spinner_annee,R.id.textViewAnnee,semestres);
        this.anneespinner.setAdapter(adapter_annee);
        txtpromotion.setText(promotionuser.get(0).getLibelle());
        this.anneespinner.setSelection(semestres.size()-1);
        anneespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (id != 0 )
                {
                    Semestre s = (Semestre) anneespinner.getSelectedItem();
                    if (s.getId() == 0) {
                        txtmoyenne.setText("");
                    }
                    else {
                        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
                        Thread moyenneEleve = new Thread(()-> {
                            //pgb.setVisibility(View.VISIBLE);
                            try {
                                moyenne = Session.user.moyenneParSemestre(Session.user.getId(),s.getId());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //pgb.setVisibility(View.INVISIBLE);
                        });
                        moyenneEleve.start();
                        try {
                            moyenneEleve.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        txtmoyenne.setText(moyenne);
                    }
                } else {
                    txtmoyenne.setText("");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
                txtmoyenne.setText("");
            }

        });
    }


    public void Deconnexion(View view) {
        Session.user = null;
        this.finish();
    }
}
