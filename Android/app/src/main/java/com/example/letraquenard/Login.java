                                                                                                                                                                                      package com.example.letraquenard;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.letraquenard.Objects.Utilisateur;
import com.example.letraquenard.util.Security.Security;
import com.example.letraquenard.util.Session;
import com.example.letraquenard.util.webservices.WebServices;

                                                                                                                                                                                      public class Login extends AppCompatActivity {
    protected ProgressBar pgb;
    protected EditText txtpwd;
    protected EditText txtmail;
    protected Utilisateur connecte;
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pgb = findViewById(R.id.prog);
        txtpwd = findViewById(R.id.editTextTextPassword);
        txtmail = findViewById(R.id.editTextTextEmailAddress);
    }
    public void Click(View view){
        new JsonTask().execute();
    }

    private class JsonTask extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute() {pgb.setVisibility(View.VISIBLE);}

        @Override
        protected Void doInBackground(Void... params) {
            WebServices.Email= txtmail.getText().toString();
            WebServices.Mdp= Security.Encrypt( txtpwd.getText().toString());

            try {
                Session.user = WebServices.Login();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pgb.setVisibility(View.INVISIBLE);
            Intent i;
            if (Session.user != null ){
                if (Session.user.getPromotion_id() != 0) {
                    i = new Intent(getApplicationContext(), EleveAccueil.class);
                    startActivity(i);
                }
                else {
                    i = new Intent(getApplicationContext(), AdminAccueil.class);
                    startActivity(i);
                }
            }else {
                CharSequence text = "Identifiants incorrects";
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        }

    }
}