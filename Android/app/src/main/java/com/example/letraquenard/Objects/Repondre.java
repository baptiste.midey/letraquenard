package com.example.letraquenard.Objects;

import java.util.ArrayList;
import java.util.List;

public class Repondre extends LeTraquenardObject {
    int id;
    boolean vraifaux;
    int question_id;
    int semestre_id;
    int utilisateur_id;

    private List<Question> questions;

    public Repondre(){}

    public Repondre(int id, boolean vraifaux, int question_id, int semestre_id,int utilisateur_id) {
        this.id = utilisateur_id;
        this.vraifaux = vraifaux;
        this.question_id = question_id;
        this.semestre_id = semestre_id;
        this.utilisateur_id = utilisateur_id;
    }

    public Repondre( boolean vraifaux, int question_id, int semestre_id,int utilisateur_id) {
        this.vraifaux = vraifaux;
        this.question_id = question_id;
        this.semestre_id = semestre_id;
        this.utilisateur_id = utilisateur_id;
    }

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    public boolean isVraifaux() {
        return vraifaux;
    }

    public void setVraifaux(boolean vraifaux) {
        this.vraifaux = vraifaux;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getSemestre_id() {
        return semestre_id;
    }

    public void setSemestre_id(int semestre_id) {
        this.semestre_id = semestre_id;
    }

    public int getUtilisateur_id() {return utilisateur_id;}

    public void setUtilisateur_id(int utilisateur_id) { this.utilisateur_id = utilisateur_id;}
}
