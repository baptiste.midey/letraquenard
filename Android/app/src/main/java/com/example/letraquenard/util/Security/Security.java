package com.example.letraquenard.util.Security;


import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Security {
    private static final String SECRET_KEY = "EscapeFromTarkovCestVachementDur";
    private static String salt = "EscapeFromTarkov";
    public static String Encrypt(String passwordToHash){
        try {
            byte[] iv = salt.getBytes(StandardCharsets.UTF_8);
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), salt.getBytes(), 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey /* 83 40 A4 EF 61 D9 73 FD 69 0A 68 0E 2A 2C 8B F4 DE 77 B7 39 D2 E7 CC 39 DF 22 6D 98 C6 15 D4 68 */,
                    ivspec /* 45 73 63 61 70 65 46 72 6F 6D 54 00 72 6B 6F 76 */);
            return bytesToHex(cipher.doFinal(passwordToHash.getBytes()));
        } catch (Exception e) {
            Log.i("Error while encrypting: " , e.toString());
        }
        return null;
    }

    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);
    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }
}