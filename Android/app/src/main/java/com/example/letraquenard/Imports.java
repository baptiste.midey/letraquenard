package com.example.letraquenard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.letraquenard.util.Files.CsvReader;
import com.example.letraquenard.util.Views.Fragments.FileChooserFragment;

public class Imports extends AppCompatActivity {
    FileChooserFragment userPicker;
    FileChooserFragment questionPicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imports);
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        userPicker = (FileChooserFragment) fragmentManager.findFragmentById(R.id.filepickerUtilisateur);
        userPicker.setCustomObjectListener(new FileChooserFragment.FileChooserFragmentListener() {
            @Override
            public void onFileChoosed() {
                ImportUser();
            }
        });
        questionPicker = (FileChooserFragment) fragmentManager.findFragmentById(R.id.filepickerall);
        questionPicker.setCustomObjectListener(new FileChooserFragment.FileChooserFragmentListener() {
            @Override
            public void onFileChoosed() {
                ImportQuestion();
            }
        });
    }

    public void Clickretour(View view) {
        this.finish();
    }

    public void ImportUser() {
        CsvReader csv = new CsvReader(getApplicationContext(),userPicker.getUri());
        csv.readUtilisateur();
    }

    public void ImportQuestion() {
        CsvReader csv = new CsvReader(getApplicationContext(),questionPicker.getUri());
        csv.readAllCsv();
    }
}