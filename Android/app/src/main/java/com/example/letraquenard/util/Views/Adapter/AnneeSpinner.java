package com.example.letraquenard.util.Views.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.letraquenard.Objects.Promotion;
import com.example.letraquenard.Objects.Semestre;

import java.util.List;

public class AnneeSpinner extends BaseAdapter {
    private List<Semestre> semestres;
    private int listItemLayoutResource;
    private int textViewAnnee;
    private LayoutInflater flater;

    public AnneeSpinner(Activity context, int listItemLayoutResource, int textViewAnnee,List<Semestre> semestres) {
        this.listItemLayoutResource = listItemLayoutResource;
        this.textViewAnnee = textViewAnnee;
        this.semestres = semestres;
        this.flater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        if (this.semestres == null) {
            return 0;
        }
        return this.semestres.size();
    }

    @Override
    public Object getItem(int position) {
        return this.semestres.get(position);
    }

    @Override
    public long getItemId(int position) {
        Semestre s = (Semestre) this.getItem(position);
        return s.getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Semestre s = (Semestre) getItem(position);
        View rowView = this.flater.inflate(this.listItemLayoutResource,null,true);
        TextView textViewAnnee= (TextView) rowView.findViewById(this.textViewAnnee);
        textViewAnnee.setText(s.getAnnee()+" "+s.getLibelle());
        return rowView;
    }
}