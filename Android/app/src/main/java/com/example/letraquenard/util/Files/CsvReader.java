package com.example.letraquenard.util.Files;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.letraquenard.Objects.Matiere;
import com.example.letraquenard.Objects.Promotion;
import com.example.letraquenard.Objects.Question;
import com.example.letraquenard.Objects.Utilisateur;
import com.example.letraquenard.R;
import com.example.letraquenard.util.webservices.WebServices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CsvReader extends AppCompatActivity {
    Context context;
    Uri fileUri;
    List<String[]> rows = new ArrayList<>(); //list = ligne   String= colonne



    public CsvReader(Context context, Uri fileUri) {
        this.context = context;
        this.fileUri = fileUri;
    }
    public void readUtilisateur(){
        Thread threadReader = new Thread(new Runnable() {
            @Override
            public void run() {
        InputStream is = null;
        String line="";
                try {
                    is = context.getContentResolver().openInputStream(fileUri);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is, Charset.forName("UTF-8"))
                );
                try {
                    reader.readLine();
                    while ((line = reader.readLine()) != null) {
                        Log.d("Utilisateur", "Line: " + line);
                        String[] tokens = line.split(";");
                        Utilisateur u = new Utilisateur();
                        u.setNom(tokens[0]);
                        u.setPrenom(tokens[1]);
                        u.setMail(tokens[2]);
                        u.setMdp(tokens[3]);
                        WebServices.Create(u);
                        Log.d("Mon activité", "Just created: " + u);
                    }

                } catch (IOException e) {
                    Log.d("MyActivity", "Error reading data file on line" + line, e);
                    e.printStackTrace();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        threadReader.start();
        try {
            threadReader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void readAllCsv(){
        Thread threadReader = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Matiere> matieres = new ArrayList<>();
                InputStream is = null;
                try {
                    is = context.getContentResolver().openInputStream(fileUri);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is, Charset.forName("UTF-8"))
                );
                String line = "";
                try {
                    reader.readLine();
                    while ((line = reader.readLine()) != null) {
                        Log.d("Utilisateur", "Line: " + line);
                        String[] tokens = line.split(";");
                        if (tokens[0] != null) {
                            Promotion p = new Promotion();
                            p.setLibelle(tokens[0]);
                            WebServices.Create(p);
                        }
                        if (tokens[1] != null) {
                            Matiere m = new Matiere();
                            m.setLibelle(tokens[1]);
                            m.setId(WebServices.Create(m));
                            matieres.add(m);
                        }
                        if (tokens[2] != null && tokens[3] != null) {
                            Question q = new Question();
                            q.setLibelle(tokens[2]);
                            Predicate<Matiere> mpred = m -> m.getLibelle().equals(tokens[3]);
                            q.setMatiere_id(matieres.stream().filter(mpred).collect(Collectors.toList()).get(0).getId());
                            WebServices.Create(q);
                        }

                    }

                } catch (IOException e) {
                    Log.d("MyActivity", "Error reading data file on line" + line, e);
                    e.printStackTrace();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        threadReader.start();
        try {
            threadReader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
