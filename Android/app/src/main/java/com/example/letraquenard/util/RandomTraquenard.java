package com.example.letraquenard.util;

import com.example.letraquenard.Objects.Enseigner;
import com.example.letraquenard.Objects.Matiere;
import com.example.letraquenard.Objects.Question;
import com.example.letraquenard.Objects.Utilisateur;
import com.example.letraquenard.util.webservices.WebServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RandomTraquenard {
    private List<Question> questions;
    private List<Utilisateur> utilisateurs;
    private Random r = new Random();

    public RandomTraquenard(List<Question> questions,List<Utilisateur> utilisateurs) {
        this.questions = questions;
        this.utilisateurs = utilisateurs;
    }

    public RandomTraquenard() {
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Utilisateur> getUtilisateurs() { return utilisateurs;}

    public void setUtilisateurs(List<Utilisateur> utilisateurs) { this.utilisateurs = utilisateurs;}

    /**** Question aléatoire ****/
    public Question questionAlea(int matiere_id) {
        List<Question> QuestionsMatiere = new ArrayList<>();
        try {
            List<Question> questions = WebServices.List(Question.class);
            Predicate<Question> pred = q->q.getMatiere_id() == matiere_id;
            QuestionsMatiere = questions.stream().filter(pred).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return QuestionsMatiere.get(r.nextInt(QuestionsMatiere.size()));
    }
    /*public Question question_alea(int id, Question q) {             /*Nouvelle Question

    }*/

    /**** Eleve aléatoire ****/
   public Utilisateur utilisateurAlea(int promotion_id) {
        List<Utilisateur> UtilisateursPromotions = new ArrayList<>();
        try {
            List<Utilisateur> utilisateurs = WebServices.List(Utilisateur.class);
            Predicate<Utilisateur> pred = u->u.getPromotion_id()== promotion_id;
            UtilisateursPromotions = utilisateurs.stream().filter(pred).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return UtilisateursPromotions.get(r.nextInt(UtilisateursPromotions.size()));
    }


}

