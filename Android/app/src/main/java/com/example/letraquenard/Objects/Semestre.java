package com.example.letraquenard.Objects;

public class Semestre extends LeTraquenardObject {
    private int id;
    private String annee;
    private String libelle;

    public Semestre(int id,String annee, String libelle) {
        this.id = id;
        this.annee = annee;
        this.libelle = libelle;
    }

    public Semestre() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnnee() { return annee;}

    public void setAnnee(String annee) { this.annee = annee;}

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
