package com.example.letraquenard;



import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.letraquenard.util.Views.Adapter.QuestionAdpater;
import com.example.letraquenard.Objects.Repondre;
import com.example.letraquenard.Objects.Utilisateur;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.example.letraquenard.Objects.Question;
import com.example.letraquenard.util.Views.RecyclerItemClickListener;
import com.example.letraquenard.util.webservices.WebServices;

public class Questions extends AppCompatActivity {
    List<Repondre> repondres = new ArrayList<>();
    List<Utilisateur>utilisateurs = new ArrayList<>();
    List<Question>questions;
    QuestionAdpater adapter;
    ProgressBar pgb;
    Question q;
    Context c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question);
         pgb = (ProgressBar)findViewById(R.id.QuestionPgb);
         c = this ;
         Intent intent = getIntent();
         if(intent.getExtras() != null){
             q = (Question) intent.getExtras().getSerializable("q");
         }

        Charger();

    }
    public void Clickretour(View view) {
        this.finish();
    }

    //new JsonTask().execute();
    public List<Utilisateur> EleveNotInterogate(){
        Predicate<Utilisateur> pred = u->u.getInterogate() == false;
        List<Utilisateur> utilisateursNotInterogate = utilisateurs.stream().filter(pred).collect(Collectors.toList());
        if( utilisateursNotInterogate.size() > 0){
            return utilisateursNotInterogate;
        }
        else if (utilisateursNotInterogate.size() == 0){
            return utilisateurs;
        }
        return null;
    }

    public void Charger() {
        questions = new ArrayList<>();
        try {

            StrictMode
                .setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (q != null) {
                            questions.add(q);
                        }
                            questions = WebServices.List(Question.class);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            thread.join();
            //populate le recycler view
            RecyclerView recyclerView = findViewById(R.id.QuestionRecycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            adapter = new QuestionAdpater(questions);
            recyclerView.setAdapter(adapter);
            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            ItemClick(position);
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {
                            try {
                                WebServices.Delete(questions.get(position).getClass(),questions.get(position).getId());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            questions.remove(questions.get(position));
                            Charger();
                        }
                    }) {
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private class JsonTask extends AsyncTask<Void,Void,Boolean> {
        @Override
        protected void onPreExecute() {pgb.setVisibility(View.VISIBLE);}

        @Override
        protected Boolean doInBackground(Void... params) {
            if (questions.size() != 0 ) {
                return true;
            }
            else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            pgb.setVisibility(View.INVISIBLE);
            if (!result) {
                Log.e("bark","aucune question");
            }
        }
    }
    protected void ItemClick(int pos){
        Intent intent = new Intent(getApplicationContext(), UpdateQuestions.class);
        Bundle b = new Bundle();
        b.putInt("key", questions.get(pos).getId()); //Your id
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
    }
}