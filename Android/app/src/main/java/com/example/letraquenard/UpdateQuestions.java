package com.example.letraquenard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.letraquenard.Objects.Question;
import com.example.letraquenard.util.webservices.WebServices;

public class UpdateQuestions extends AppCompatActivity {
   Question question;
    TextView textQuestionUpadate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_question);

        try {
            int i =getIntent().getIntExtra("key",-1);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        question = WebServices.Get(Question.class, i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Charger();
    }

    private void Charger() {
        findViewById(R.id.textQuestionUpadate);
        textQuestionUpadate = findViewById(R.id.textQuestionUpadate);
        textQuestionUpadate.setText(question.getLibelle());

    }

    public void UpdateQuestions(View view){
        Thread updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                question.setLibelle(textQuestionUpadate.getText().toString());
                try {
                    WebServices.Update(question, question.getId());
                    Intent refresh = new Intent(getApplicationContext(), Questions.class );
                    refresh.putExtra("q", question);
                    startActivity(refresh);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        updateThread.start();
        try {
            updateThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
