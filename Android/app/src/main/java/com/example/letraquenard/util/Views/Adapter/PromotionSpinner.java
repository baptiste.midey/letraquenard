package com.example.letraquenard.util.Views.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.letraquenard.Objects.Promotion;

import java.util.List;

public class PromotionSpinner extends BaseAdapter {
    private List<Promotion> promotions;
    private int listItemLayoutResource;
    private int textViewPromotion;
    private LayoutInflater flater;

    public PromotionSpinner(Activity context, int listItemLayoutResource, int textViewPromotion, List<Promotion> promotions) {
        this.listItemLayoutResource = listItemLayoutResource;
        this.textViewPromotion = textViewPromotion;
        this.promotions = promotions;
        this.flater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        if (this.promotions == null) {
            return 0;
        }
        return this.promotions.size();
    }

    @Override
    public Object getItem(int position) {
        return this.promotions.get(position);
    }

    @Override
    public long getItemId(int position) {
        Promotion p = (Promotion) this.getItem(position);
        return p.getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Promotion p = (Promotion) getItem(position);
        View rowView = this.flater.inflate(this.listItemLayoutResource,null,true);
        TextView textViewPromotion = (TextView) rowView.findViewById(this.textViewPromotion);
        textViewPromotion.setText(p.getLibelle());
        return rowView;
    }
}